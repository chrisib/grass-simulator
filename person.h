#ifndef PERSON_H
#define PERSON_H

#include <thingy.h>
#include <field.h>

/*!
 * \brief A human being that walks through the field
 *
 * The size of the person is based on data from Clauser et al. and should be
 * representative of a modern-day adult male.
 *
 * When initialized the person's exact foot dimensions and stride size are randomly
 * determined, following a normal distribution around the provided averages.
 *
 * Note: Clauser et al. did not measure the width of the foot, so these numbers
 * are my own approximation.
 *
 * Note: Clauser et al. did not indicate the distance between the feet when walking
 * so I have used the measurements provided for the width of the chest, assuming that
 * the feet are spaced chest-breadth apart when walking.
 *
 * Note: we approximate the footprints as rectangles when traversing the field
 */
class Person : public Thingy
{
public:
    Person();

    virtual void traverse(Field &field);

    // Clauser et al. (http://www.dtic.mil/dtic/tr/fulltext/u2/710622.pdf)
    static constexpr double AVG_FOOT_LENGTH_CM = 24.78;
    static constexpr double AVG_FOOT_WIDTH_CM = 10;
    static constexpr double AVG_STRIDE_LENGTH_CM = 76;
    static constexpr double AVG_FOOT_SEPARATION_CM = 33.23;
    static constexpr double AVG_MASS = 66.52;

private:
    double footLength;
    double footWidth;
    double stanceWidth;
    double strideLength;
};

#endif // PERSON_H
