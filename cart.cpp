#include "cart.h"
#include <horse.h>
#include <random.h>
#include <iostream>

using namespace cv;
using namespace std;

static Random midpoint(0,5);
static Random rWheelThickness(Cart::AVG_WHEEL_THICKNESS_CM,0.5);
static Random rAxleWidth(Cart::AVG_AXLE_WIDTH_CM,5);
static Random rMass(Cart::AVG_MASS_PER_AXLE, 200);

Cart::Cart(int axles, int horses)
{
    numAxles = axles;
    this->wheelThickness = rWheelThickness.next();
    axleWidth = rAxleWidth.next();
    mass = rMass.next() * axles;

    for(int i=0; i<horses; i++)
        this->horses.push_back(Horse());
}

void Cart::traverse(Field &field)
{
    // because of a glitch with rounding errors we need to randomly flip the math around
    // otherwise we get one wheel track being much wider than the other due to rounding
    // but this conveniently mimics whether the cart is coming or going, which is fine
    int comingGoing = rand() % 2 ? -1:1;
    double cartMid = field.width()/2 + comingGoing*midpoint.next();

    Rect2d leftWheel(cartMid - axleWidth/2, 0, wheelThickness, field.length());
    Rect2d rightWheel(cartMid + axleWidth/2, 0, wheelThickness, field.length());

    // assume the wheel applies pressure in a rectangular area proportional to the width of the wheel
    double pressure = mass * 9.81 / pow(4*wheelThickness, 2) / 2 / numAxles;
    //cout << "Cart: " << pressure << "N/cm^2" << endl;

    for(int i=0; i<numAxles; i++)
    {
        field.trample(leftWheel, pressure);
        field.trample(rightWheel, pressure);
    }

    int horseOffset = horses.size() % 2 == 0 ?
                axleWidth/2 - Horse::AVG_FOOT_SPACING_CM : 0;
    for(size_t i=0; i<horses.size(); i++)
    {
        ((Quadruped*)(&horses[i]))->traverse(field,cartMid + (i % 2 == 0 ? -1:1) * horseOffset);
    }
}
