#include "person.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <algorithm>
#include <random.h>
#include <iostream>
#include <cart.h>

using namespace cv;
using namespace std;

// people walk down the middle of the road with a standard deviation of 50cm
static Random rMidpointGenerator(0,50);

// Clauser et al. (http://www.dtic.mil/dtic/tr/fulltext/u2/710622.pdf)
static Random rFootLength(Person::AVG_FOOT_LENGTH_CM,1.0);
static Random rFootWidth(Person::AVG_FOOT_WIDTH_CM,1); // guess at deviation
static Random rFootSeparation(Person::AVG_FOOT_SEPARATION_CM,2.53);
static Random rStrideLength(Person::AVG_STRIDE_LENGTH_CM, 10); // guess at deviation
static Random rMass(Person::AVG_MASS,8.70);

Person::Person()
{
    strideLength = rStrideLength.next();
    stanceWidth = rFootSeparation.next();
    footLength = rFootLength.next();
    footWidth = rFootWidth.next();
    mass = rMass.next();
}

void Person::traverse(Field &field)
{
    // divide people into 3 even groups: people who walk down the middle of the road
    // and people who walk in one of the two extant wheel ruts
    int offset = (rand() % 3) -1;
    double mid = field.width()/2 + rMidpointGenerator.next() + offset * Cart::AVG_AXLE_WIDTH_CM/2.0;

    double front = Random::drand() * -strideLength;
    double back = front-strideLength/2;

    double pressure = mass * 9.81 / (footLength * footWidth);
    //cout << "Person: " << pressure << "N/cm^2" << endl;

    int lr = rand() % 2 ? -1:1;
    while(back < field.length())
    {
        // back left corner of each foot of the four feet (front/back, left/right)
        Point2d p(mid + lr*stanceWidth/2, front);
        Point2d q(mid - lr*stanceWidth/2, back);

        field.trample(Rect(p,Point(p.x+footWidth,p.y+footLength)),pressure);
        field.trample(Rect(q,Point(q.x+footWidth,q.y+footLength)),pressure);

        back += strideLength;
        front += strideLength;
    }
}
