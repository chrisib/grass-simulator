#include "horse.h"
#include <random.h>
#include <iostream>

using namespace cv;
using namespace std;

// standard deviations are an estimate made by me; I couldn't find any hard data for these values
static Random rHoofSise(Horse::AVG_HOOF_DIAMETER_CM, 2);
static Random rStrideLength(Horse::AVG_STRIDE_LENGTH_CM,30);
static Random rStanceWidth(Horse::AVG_FOOT_SPACING_CM, 5);
static Random rLimbSpacing(Horse::AVG_LIMB_SPACING_CM, 10);
static Random rMass(Horse::AVG_MASS, 50);

// assume a standard deviation of 50cm from the middle of the road
static Random midpoint(0,50);

Horse::Horse() : Quadruped()
{
    strideLength = rStrideLength.next();
    footDiameter = rHoofSise.next();
    stanceWidth = rStanceWidth.next();
    stanceLength = rLimbSpacing.next();
    mass = rMass.next();
}

void Horse::traverse(Field &field)
{
    Quadruped::traverse(field, field.width()/2 + midpoint.next());
}
