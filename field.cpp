#include "field.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Field::Field(int length, int width)
{
    image = cv::UMat(length * 100, width * 100, CV_8UC1);
    drawingMask = cv::UMat(length * 100, width * 100, CV_8UC1);
    tmp = cv::UMat(length * 100, width * 100, CV_8UC1);

    cv::rectangle(image,cv::Point(0,0),cv::Point(image.cols-1, image.rows-1),cv::Scalar(255,255,255),-1);
    rectangle(drawingMask, Point(0,0),Point(drawingMask.cols-1, drawingMask.rows-1),cv::Scalar(0,0,0));

    deathRateWhenTrampled = 0.01;
    pressureResistence = 500;
}

void Field::trample(cv::Point p, double diameter, double pressure)
{
    circle(drawingMask,p,(int)(diameter/2),Scalar(255,255,255),-1);
    applyMask(pressure);
    circle(drawingMask,p,(int)(diameter/2),Scalar(0,0,0),-1);
}

void Field::trample(cv::Rect r, double pressure)
{
    rectangle(drawingMask,r.tl(), r.br(),Scalar(255,255,255),-1);
    applyMask(pressure);
    rectangle(drawingMask,r.tl(), r.br(),Scalar(0,0,0),-1);
}

void Field::applyMask(double pressure)
{
    // extract the footprint and store it in tmp
    bitwise_and(image,drawingMask,tmp);

    // remove the old footprint area
    bitwise_not(drawingMask,drawingMask);
    bitwise_and(image,drawingMask,image);

    double killRate = deathRateWhenTrampled;
    if(pressure > pressureResistence)
    {
        killRate *= pressure/pressureResistence;
    }

    // trample the ground killing off the grass
    tmp.convertTo(tmp,tmp.type(),1.0 - killRate);

    // merge tmp and the original drawing back together
    bitwise_or(image,tmp,image);

    // undo the inversion to the mask so it's back to how we started
    bitwise_not(drawingMask,drawingMask);
}

void Field::regrow(int day)
{
    image.convertTo(image,image.type(),1.0,regrowthRate(day));
}

double Field::regrowthRate(int day)
{
    // use a cos curve to calculate the growth rate based on seasons
    double radians = (double)day/365.0 * 2 * M_PI;
    double range = MAX_GROWTH - MIN_GROWTH;

    double c = -cos(radians);
    double growth = (c+1)/2.0 * range + MIN_GROWTH;
    return growth;
}

string Field::season(int day)
{
    day += 365/8;
    day = day % 365;

    double progress = day/365.0;
    if(progress < 0.25)
        return "Winter";
    else if(progress < 0.5)
        return "Spring";
    else if(progress < 0.75)
        return "Summer";
    else
        return "Autumn";
}
