#ifndef CART_H
#define CART_H

#include <thingy.h>
#include <horse.h>
#include <vector>

/*!
 * \brief A wheeled, horse-drawn cart
 *
 * When initialized the team of horses is created, and the cart specs are
 * determined following a normal distribution around the average.
 *
 * We assume all carts are roughly 127cm wide (which roughly corresponds to
 * the width of a Roman chariot, or by non-coincidence, modern railway gauge).
 *
 * The actual width of the wheelbase is random, but with a mean of 127cm.
 *
 * Unlike horses and people, the cart has a smaller standard deviation, assuming that
 * the cart will follow the existing path more closely than people or horses, who are
 * more maneuverable and can move laterally.
 *
 * Note: when traversing the field, we trample a rectangular region for each wheel
 * (2* number of axles), lying exactly on top of each other.  Since the cart is going
 * in a straight line this should be a safe assumption.
 *
 * Note: when traversing the field we also trigger the team of horses to traverse.  If
 * we have a single horse it walks centered between the wheels.  If we have a team of
 * horses we assume they are in two lines abreast.
 */
class Cart : public Thingy
{
public:
    /*!
     * \brief Create a new cart
     * \param axles The number of axles the cart has
     * \param horses The number of horses drawing the cart
     *
     * The cart's width is determined by the number of horses / 2, rounding up.
     *
     * The cart's horses automatically traverse the field with the cart, leaving tracks
     */
    Cart(int axles = 1, int horses = 1);

    /*!
     * \brief Walk through the field in a straight line, roughly centered in the middle
     * \param field The field whose grass we're trampling
     */
    virtual void traverse(Field &field);

    // assume the wheels are about 1" thick on average
    static constexpr double AVG_WHEEL_THICKNESS_CM = 2.54;

    // approx. standard railway gauge; normal width for a horse-drawn cart
    static constexpr double AVG_AXLE_WIDTH_CM = 127;

    // kg per axle; complete guess
    static constexpr double AVG_MASS_PER_AXLE = 1000;

private:
    int numAxles;
    double wheelThickness;
    double axleWidth;

    std::vector<Horse> horses;
};

#endif // CART_H
