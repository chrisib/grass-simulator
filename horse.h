#ifndef HORSE_H
#define HORSE_H

#include <quadruped.h>

/*!
 * \brief A single horse that can traverse a field
 *
 * The horse's size is based on some cursary research.  I suspect the numbers I
 * found are better representative of a modern racing or trail horse, and not
 * necessarily a historically-accurate medieval work horse.  However, the numbers
 * should be reasonable.
 *
 * When initialized the horse's exact size are randomly determined, following a
 * normal distribution around the averages indicated below.
 *
 * Note: we approximate the feet to be circles when traversing the field
 */
class Horse : public Quadruped
{
public:
    Horse();

    /*!
     * \brief Choose a random path through the field (following a normal distribution centred
     *        at the middle of the field) and walk along that path.
     * \param field
     */
    virtual void traverse(Field &field);

    // values based on
    // https://www.horseloversmath.com/shoulder-angle-relate-stride-length/
    // and
    // https://www.fhwa.dot.gov/environment/recreational_trails/publications/fs_publications/07232816/page05.cfm
    static constexpr double AVG_HOOF_DIAMETER_CM = 13.0;  //diameter of the foot, assuming circular
    static constexpr double AVG_STRIDE_LENGTH_CM = 151; // length of a stride
    static constexpr double AVG_FOOT_SPACING_CM = 46;   // distance between feet in a pair
    static constexpr double AVG_LIMB_SPACING_CM = 180;  // distance from front to back foot
    static constexpr double AVG_MASS = 600; // largely a guess; adult horses are typically 350-1000kg,
};

#endif // HORSE_H
