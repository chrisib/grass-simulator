#include "random.h"

Random::Random(double mean, double deviation) : distribution(mean, deviation)
{

}

double Random::next()
{
    return distribution(generator);
}

double Random::drand()
{
    return (double)rand() / RAND_MAX;
}

int Random::percent()
{
    return rand() % 100;
}

int Random::dice(int n)
{
    return (rand() % n) +1;
}
