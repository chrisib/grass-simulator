#ifndef QUADRUPED_H
#define QUADRUPED_H

#include <thingy.h>

class Quadruped : public Thingy
{
public:
    Quadruped();
    virtual ~Quadruped();

    /*!
     * \brief Follow a pre-chosen path through the field
     * \param field
     * \param middle The distance from the left edge of the field to traverse along (in cm)
     */
    virtual void traverse(Field &field, double middle);

protected:
    double strideLength;
    double stanceLength;
    double stanceWidth;
    double footDiameter;
};

#endif // QUADRUPED_H
