#ifndef FIELD_H
#define FIELD_H

#include <opencv2/core.hpp>

/*!
 * \brief A grassy field that thingies will walk through to leave trails
 */
class Field
{
public:
    /*!
     * \brief Create a new, square field with the given dimensions in meters
     * \param length
     * \param width
     */
    Field(int length, int width);

    /*!
     * \brief The matrix we use to represent the field
     *
     * Each pixel represents a 1cm by 1cm area of ground
     */
    cv::UMat image;

    /*!
     * \brief Step on a circular area of ground and trample the grass in it
     * \param p
     * \param diameter
     * \param pressure The pressure in N/cm^2 applied to the trampled region
     */
    void trample(cv::Point p, double diameter, double pressure);

    /*!
     * \brief Step on a rectangular area of ground and trample the grass in it
     * \param r
     * \param pressure The pressure in N/cm^2 applied to the trampled region
     */
    void trample(cv::Rect r, double pressure);

    /*!
     * \brief Regrow the grass in the field
     * \param day
     */
    void regrow(int day);

    /*!
     * \brief The length of the field in cm
     * \return
     */
    int length() {return image.rows;}

    /*!
     * \brief The width of the field in cm
     * \return
     */
    int width() {return image.cols;}

    /*!
     * \brief Calculate the growth rate of grass assuming that the rate follows a sine curve between min and max
     * \param day The day of the year (starting in winter with minimal growth)
     * \return
     */
    double regrowthRate(int day);

    /*!
     * \brief The name of the current simulation season
     * \param day
     * \return
     */
    std::string season(int day);

    /*!
     * \brief A 0-1 value indicating how much of the grass dies when trampled upon
     */
    double deathRateWhenTrampled;

    /*!
     * \brief How many pascals does it take to kill the grass?
     */
    double pressureResistence;

    /*!
     * \brief Grass grows a minimum of about 1.5mm per day (0.06")
     */
    static constexpr double MIN_GROWTH = 1.5;

    /*!
     * \brief Grass grows a maximum of about 5mm per day (0.2")
     */
    static constexpr double MAX_GROWTH = 5.08;

private:
    void applyMask(double pressure);

    cv::UMat drawingMask;
    cv::UMat tmp;
};

#endif // FIELD_H
