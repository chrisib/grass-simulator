#ifndef SHEEP_H
#define SHEEP_H

#include <quadruped.h>


class Sheep : public Quadruped
{
public:
    Sheep();

    virtual void traverse(Field &field);

    static constexpr double AVG_MASS = 85; //kg; range is 40-130kg
    static constexpr double AVG_STANCE_LENGTH = 75; //cm; length is 90-140, minus ~40cm for the face [guess]
    static constexpr double AVG_STANCE_WIDTH = 30;  //cm; guess, based on pictures of sheep assuming a body length in the 75-100cm range
    static constexpr double AVG_FOOT_SIZE = 3;      //cm; guess based on pictures of sheep hooves
    static constexpr double AVG_STRIDE = 60;        //cm; slightly less than the stance length
};

#endif // SHEEP_H
